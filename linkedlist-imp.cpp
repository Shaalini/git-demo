#include <stdio.h>
#include <iostream>
using namespace std;

struct Node
{
    public:
        int data;
        Node *next;
};

class LINKED_LIST
{
    private:
        Node *head,*tail;
    public:
        LINKED_LIST()
        {
            head = NULL;
            tail = NULL;
        }

        int INSERT(int data)
        {
            Node *temp = new Node;
            temp->data = data;
            temp->next = NULL;
            if(head == NULL)
            {
                //Initialize the I Node
                head = temp;
                tail = head;
                return 1;
            }
            else
            {
                tail->next = temp;
                tail = temp;
                return 1;
            }
            return 0;
            
        }
        int PRINT()
        {
            Node *traverse = head;
            cout<<"\n LIST :";
            while(traverse != NULL)
            {
                cout<<traverse->data<<" ";
                traverse = traverse->next;
            }
            cout<<"\n";
        }

        int SEARCH(int data)
        {
            Node *ptr = head;
            while(ptr != NULL)
            {
                if(ptr->data == data)
                {
                    cout<<data<<"is in the list\n";
                    return 1;
                }
                ptr = ptr->next;
            }
            cout<<data<<"is not in the list\n";
            return 0;
        }

        int DELETE(int data)
        {
            if(head->data == data)
            {
                head = head->next;
                cout<<"\nDeleted "<<data;
                return 1;
            }
            Node *ptr = head;
            Node *prev = head;
            while(ptr != NULL)
            {
                if(ptr->data == data)
                {
                    prev->next = ptr->next;
                    cout<<"\nDeleted "<<data;
                    if(ptr == tail)
                    {
                        tail = prev;
                    }
                    return 1;
                }
                prev = ptr;
                ptr = ptr->next;
            }
            cout<<"\nNot Deleted "<<data<<" Not in the list";
            return 0;
        }

        int RETURN_HEAD()
        {
            return head->data;
        }

        int RETURN_TAIL()
        {
            return tail->data;
        }
};
void returnHello()
{
    cout<<"Hello World";
}
int addition(int a, int b)
{
    return a+b;
}
int main()
{
    LINKED_LIST LL;
    for(int i = 0;i < 10;i++)
    {
        LL.INSERT(i);
    }
    LL.SEARCH(5);
    LL.DELETE(0);
    LL.PRINT();
    LL.DELETE(1);
    LL.DELETE(9);
    //cout<<LL.RETURN_HEAD()<<" "<<LL.RETURN_TAIL();
    return 0;    
}